<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Exercícios GIT</h2>
</div>

Basicamente a tarefa será criar um repositório remoto em sua conta to GitLab, e enviar um repositório local seu que possua os seguintes requisitos:
 - Diversos Commits
 - 2 ou mais Branchs
 - Um merge sem conflito
 - Um merge com conflito corrigido
